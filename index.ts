import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";

// Define the Features interface
interface Features {
  ec2: boolean;
  lambda: boolean;
  s3: boolean;
  sns: boolean;
  rds: boolean;
  kms: boolean;
  cloudformation: boolean;
  waf: boolean;
  alb: boolean;
  dynamodb: boolean;
  route53: boolean;
}

const out: any = {}

const config = new pulumi.Config()
const features: Features = config.requireObject<Features>("features");

const identity = pulumi.output(aws.getCallerIdentity({}));

// Export the ARN of the current AWS caller identity
export const arn = identity.arn;

// Keep reference to your existing IAM role
const role = aws.iam.Role.get("existingRole", "Tenant-Owner");

if (features.ec2) {
  // AWS EC2 - Create a new EC2 instance for validating ec2:* permission
  const size = "t2.micro";

  // Get the Ubuntu 2022 LTS AMI
  const ami = aws.ec2.getAmi({
    filters: [
      {
        name: "name",
        values: ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"],
      },
      {
        name: "virtualization-type",
        values: ["hvm"],
      },
    ],
    mostRecent: true,
    owners: ["099720109477"], // Canonical
  });

  const ec2Instance = new aws.ec2.Instance("web-server", {
    instanceType: size,
    vpcSecurityGroupIds: [], // add your security groups here
    ami: ami.then((ami) => ami.id),
  });

  out.ec2 = ec2Instance
}

if (features.lambda) {
  // AWS CloudWatch Logs - Create a new Cloudwatch log group for validating
  // cloudwatch:* permission
  const logGroup = new aws.cloudwatch.LogGroup("apiAccessLogGroup");

  // Create a custom IAM role for Lambda
  const lambdaRole = new aws.iam.Role("lambdaRole", {
    assumeRolePolicy: JSON.stringify({
      Version: "2012-10-17",
      Statement: [
        {
          Action: "sts:AssumeRole",
          Principal: {
            Service: "lambda.amazonaws.com",
          },
          Effect: "Allow",
        },
      ],
    }),
  });

  // Attach necessary permissions to the role
  const lambdaPolicyAttachment = new aws.iam.RolePolicyAttachment("lambdaPolicyAttachment", {
    role: lambdaRole,
    policyArn: aws.iam.ManagedPolicies.AWSLambdaBasicExecutionRole,
  });

  // AWS Lambda - Create a new Lambda function and set our previously created IAM role
  const lambdaFunc = new aws.lambda.Function("apiAccessLambdaFunc", {
    role: lambdaRole.arn,
    handler: "index.handler",
    runtime: "nodejs18.x",
    code: new pulumi.asset.AssetArchive({
      "index.js": new pulumi.asset.StringAsset(
        `exports.handler = async function (event, context) {
         console.log("log from lambda function");
         return context.logStreamName;
       }`
      ),
    }),
  });

  // Enable CloudWatch logging for previously created Lambda function
  new aws.cloudwatch.LogSubscriptionFilter("lambdaCloudwatchFilter", {
    logGroup: logGroup.name,
    destinationArn: lambdaFunc.arn,
    filterPattern: "",
  });
  out.lambda = lambdaFunc
}

if (features.cloudformation) {
  // AWS CloudFormation - Initiate a CloudFormation stack for validating
  // cloudformation:* permission
  const cfn_stack = new aws.cloudformation.Stack("cfn_stack", {
    name: 'test-stack',
    templateBody: `
Resources:
  MyS3Bucket:
    Type: "AWS::S3::Bucket"
    Properties:
      BucketName: "my-simple-s3-bucket-dair-atir-2023"
  `,
  });

  out.cloudformation = cfn_stack
}

if (features.sns) {

  // Create a new SNS topic
  const snsTopic = new aws.sns.Topic("snsTopic");

  out.sns = snsTopic
}

if (features.rds) {
  // Create a new security group for RDS
  const sgRds = new aws.ec2.SecurityGroup("sgRds", {
    ingress: [{
      protocol: "tcp",
      fromPort: 3306,
      toPort: 3306,
      cidrBlocks: ["0.0.0.0/0"],
    }],
    egress: [{
      protocol: "-1",
      fromPort: 0,
      toPort: 0,
      cidrBlocks: ["0.0.0.0/0"],
    }]
  });

  // Create a new RDS instance
  const rdsInstance = new aws.rds.Instance("rdsInstance", {
    instanceClass: "db.t2.micro",
    identifier: "rds-instance",
    engine: "mysql",
    engineVersion: "5.7",
    allocatedStorage: 20,
    storageType: "gp2",
    username: "admin",
    password: "password",
    multiAz: false,
    publiclyAccessible: true,
    vpcSecurityGroupIds: [sgRds.id],
    skipFinalSnapshot: true
  });
  out.rds = rdsInstance;
}

if (features.kms) {
  const kmsKey = new aws.kms.Key("myKmsKey", {
    description: "KMS key for encryption",
    deletionWindowInDays: 7,
  });
  out.kms = kmsKey;
}

if (features.s3) {
  // Create an AWS resource (S3 Bucket)
  const bucket = new aws.s3.Bucket("my-bucket");
  out.s3 = bucket
}

// Phase I

// Web Application Firewall (WAF)
if (features.waf) {
  const webAcl = new aws.wafv2.WebAcl("myWebAcl", {
    scope: "REGIONAL",
    defaultAction: {
      allow: {}
    },
    visibilityConfig: {
      cloudwatchMetricsEnabled: false,
      metricName: "webAclMetric",
      sampledRequestsEnabled: false
    }
  });

  out.waf = webAcl;
}

// Application Load Balancer (ALB)
if (features.alb) {
  const alb = new awsx.lb.ApplicationLoadBalancer("myAlb", {
    // Add your ALB settings here
  });
  out.alb = alb;
}

// DynamoDB
if (features.dynamodb) {
  const dynamoTable = new aws.dynamodb.Table("myTable", {
    attributes: [{
      name: "id",
      type: "N"
    }],
    hashKey: "id",
    readCapacity: 5,
    writeCapacity: 5,
  });
  out.dynamodb = dynamoTable;
}

// Route 53
if (features.route53) {
  const zone = new aws.route53.Zone("myZone", {
    name: "example.com",
  });
  out.route53 = zone;
}

export const outputs = out
